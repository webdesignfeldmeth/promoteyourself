#!/bin/bash

echo "0: laravel installieren"
echo "1: Rechte setzen"
echo "2: Benutzerverwaltung einrichten"
echo "3: Controller, Model und DB-Tabelle anlegen"
echo "4: Datenbank migrieren"
echo "5: Cache leeren"
echo "6: Laravel Version checken"
echo "7: Spalte in Datenbanktabelle hinzufügen"

echo ""

echo "Bitte wählen:"

read OPTION

case "$OPTION" in
	0)
	echo "Projekt:"
	read PROJECT
	echo "Laravel Version, z.B. 8.2"
	read LARAVELV
	composer create-project --prefer-dist laravel/laravel $PROJECT "$LARAVELV.*"
	cd $PROJECT
	sudo chown -R www-data storage
	sudo chgrp -R www-data storage bootstrap/cache 
	sudo chmod -R ug+rwx storage bootstrap/cache
	sudo chmod -R 777 storage bootstrap/cache
	php artisan key:generate
	;;
	1)
	sudo chown -R www-data:www-data storage
	sudo chgrp -R www-data storage bootstrap/cache 
        sudo chmod -R ug+rwx storage bootstrap/cache
	sudo chmod -R 777 storage bootstrap/cache
	;;
	2)
	composer require laravel/ui
	php artisan ui vue --auth
	;;
	3)
	echo "Name für die Erstellung (Einzahl)"
	read CONFIGNAME
	php artisan make:controller $CONFIGNAME"Controller" -r --model=$CONFIGNAME
	read -p "Datenbanktabelle anlegen? <y/N>" CREATEDB
	if [ $CREATEDB == "y" ]
		then
		php artisan make:migration create_${CONFIGNAME,,}s_table
	fi
	;;
	4)
	php artisan migrate
	;;
	5)
	php artisan cache:clear
	;;
	6)
	php artisan --version
	;;
	7)
	echo "Spaltenname"
	read COLUMN
	echo "Tabelle"
	read TABLE
	php artisan make:migration "add_"$COLUMN"_column_to_"$TABLE"_table" --table=$TABLE
	;;
esac
