@extends('layouts.app')

@section('content')
	<section>
		<form method="POST" action="/profil/firma-oder-verein/speichern">
			@csrf
			@include("layouts.form.input", [
						"type" => "hidden",
						"name" => "companyId",
						"id" => "company-id",
						"value" => $company["id"]
					])
			<div class="columns-two">
				<div>
					@include("layouts.form.input", [
						"type" => "file",
						"name" => "companyLogoUpload",
						"id" => "logo-upload",
						"label" => "Firmen- oder Vereinslogo"
					])
					@include("layouts.form.input", [
						"label" => "Firmen-/Vereinsname",
						"name" => "companyName",
						"id" => "company-name",
						"required" => true,
						"value" => $company["companyName"]
					])
					@include("layouts.form.select", [
						"label" => "Betriebsgröße",
						"name" => "companySize",
						"id" => "company-size",
						"options" => [
							"1-10",
							"11-50",
							"51-100",
							"101-500",
							"501-1000",
							"mehr als 1001"
						],
						"preSelect" => $company["companySize"]
					])
				</div>
				<div>
					@include("layouts.form.input", [
						"label" => "Adresse",
						"name" => "companyAddress",
						"id" => "company-address",
						"required" => true,
						"value" => $company["companyAddress"]
					])
					@include("layouts.form.input", [
						"label" => "Postleizahl",
						"name" => "companyPostcode",
						"id" => "company-postcode",
						"required" => true,
						"value" => $company["companyPostcode"]
					])
					@include("layouts.form.input", [
						"label" => "Ort",
						"name" => "companyCity",
						"id" => "company-city",
						"required" => true,
						"value" => $company["companyCity"]
					])
					@include("layouts.form.input", [
						"label" => "Telefon",
						"name" => "companyPhone",
						"id" => "company-phone",
						"value" => $company["companyPhone"]
					])
					@include("layouts.form.input", [
						"label" => "Email",
						"type" => "email",
						"name" => "companyEmail",
						"id" => "company-email",
						"value" => $company["companyEmail"]
					])
				</div>
			</div>
			@include("layouts.form.input", [
				"type" => "submit",
				"name" => "createNew",
				"id" => "create-new",
				"value" => "Speichern",
				"class" => "btn btn-save"
			])
		</div>
	</form>
	</section>
@endsection