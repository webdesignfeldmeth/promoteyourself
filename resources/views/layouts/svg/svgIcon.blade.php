<svg class="icon {{$class??""}}" focusable="false">
	<title>{{$title??""}}</title>
	<use xlink:href="#svg-{{$icon??"home"}}" title="{{$title??"Home"}}"></use>
</svg>