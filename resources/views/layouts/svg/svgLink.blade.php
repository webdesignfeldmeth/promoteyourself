<a {{$onclick??""}} href="{{route($route, $params)??"/"}}">
	@include("layouts.svg.svgIcon", [
		"icon" => $icon??"",
		"title" => $title??"",
		"class" => $class??""
	])
</a>