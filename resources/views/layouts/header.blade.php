<header class="fullsize with-area-end">
	<section>
		<h1>{{$headline??"promote yourself"}}</h1>
	</section>
</header>
<div class="area-end down grey"></div>