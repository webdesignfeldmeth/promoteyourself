<div class="form-group">
	<label for={{$id??""}}>{{$label??""}}</label>
	<select name="{{$name??""}}" class="form-control" @if(isset($required) && $required) required @endif>
		<option>Bitte wählen ...</option>
		@if(isset($options)):
			@foreach ($options as $key => $option):
				<option value="{{$key}}" @if(isset($preSelect) && $key == $preSelect) selected @endif>{{$option}}</option>
			@endforeach
		@endif
	</select>
</div>
