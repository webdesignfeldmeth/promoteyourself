<div class="form-group">
	<label for={{$id??""}}>{{$label??""}}</label>
	@if(isset($required) && $required)<span class="required"></span>@endif
	<input
		type="{{$type??"text"}}" 
		name="{{$name??""}}"
		id="{{$id??""}}"
		class="form-control @error('{{$id??""}}') is-invalid @enderror {{$class??""}}" 
		value="{{$value??""}}"
		@if(isset($required) && $required) required @endif
		@if(isset($attrs) && !empty($attrs))
			@foreach($attrs as $key => $attr)
				@if($attr != "no")
				{{$key}} = {{$attr}}
				@endif
			@endforeach
		@endif
		/>
	@error('{{$id??""}}')
    	<div class="alert alert-danger">{{ $message??"Es gab einen Fehler" }}</div>
	@enderror
</div>
