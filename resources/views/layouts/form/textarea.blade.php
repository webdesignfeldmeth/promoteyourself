<div class="form-group">
	<label for={{$id??""}}>{{$label??""}}</label>
	<textarea
		name="{{$name??""}}"
		id="{{$id??""}}"
		class="form-control @error('{{$id??""}}') is-invalid @enderror {{$class??""}}"
		cols="{{$cols??""}}"
		rows="{{$rows??""}}"
		@if(isset($required) && $required) required @endif
		>{{$value??""}}</textarea>
	@error('{{$id??""}}')
    	<div class="alert alert-danger">{{ $message??"Es gab einen Fehler" }}</div>
	@enderror
</div>
