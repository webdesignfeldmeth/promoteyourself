@if(isset($youtube) && $youtube != "")
	<iframe
		width="{{$width??"100%"}}"
		height="{{$height??"400px"}}"
		src="https://www.youtube.com/embed/{{$youtube}}
				?controls={{$controls??0}}
				&amp;rel=0
				&amp;showinfo=0
				&amp;modestbranding=1"
		allowfullscreen
		class="video"
		>
	</iframe>
@else
	Es wurde noch kein Video hinterlegt
@endif