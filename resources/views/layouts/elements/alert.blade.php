<div class="alert alert-{{$type??"warning"}} {{$size??""}}">
	@if(isset($headlineLarge) && $headlineLarge) <h2>{{$headlineLarge??""}}</h2> @endif
	@if(isset($headlineMedium) && $headlineMedium) <h3>{{$headlineMedium??""}}</h3> @endif
	@if(isset($headlineSmall) && $headlineSmall) <h4>{{$headlineSmall??""}}</h4> @endif
	<p>{!! nl2br(e($message)) !!}</p>
</div>