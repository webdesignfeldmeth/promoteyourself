@if(isset($value) && $value != "")
	@php
		$params = isset($params) ? $params : ""
	@endphp
	<a 
		href="@if(isset($route) && $route != ""){{route($route, $params)}}@endif" 
		class="@if(isset($class) && $class != ""){{$class}}@endif">
		{{$value}}
	</a>
@endif