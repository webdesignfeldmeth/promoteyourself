<nav class="fullsize">
	<div class="nav-inner">
		<div>
			<a class="navbar-brand" href="{{ url('/') }}">
				{{ config('app.name', 'Laravel') }}
			</a>
		</div>
		<div>
			{{-- <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="{{ __('Toggle navigation') }}">
				<span class="navbar-toggler-icon"></span>
			</button> --}}

			@guest
				@if (Route::has('login'))
					<a class="btn btn-login spacing" href="{{ route('login') }}">{{ __('Login') }}</a>
				@endif
				
				@if (Route::has('register'))
					<a class="btn btn-register spacing" href="{{ route('register') }}">{{ __('Register') }}</a>
				@endif
			@else
					<a class="btn" href="{{route("profil")}}" role="button">
						{{ Auth::user()->name }}
					</a>

					<a class="btn btn-login" href="{{ route('logout') }}"
					onclick="event.preventDefault();
									document.getElementById('logout-form').submit();">
						{{ __('Logout') }}
					</a>
					<form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
						@csrf
					</form>
				</li>
			@endguest
		</div>
	</div>
</nav>
<div class="area-end up orange"></div>