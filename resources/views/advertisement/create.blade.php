@extends('layouts.app')

@section('content')
	<section>
		<form method="POST" action="{{route("neue-anzeige-speichern")}}">
			@csrf
			<div class="columns-two">
				<div>
					@include("layouts.form.select", [
						"label" => "Firma oder Verein",
						"name" => "companyId",
						"id" => "company-id",
						"options" => $companyNames
					])
					@include("layouts.form.input", [
						"label" => "Anzeigentitel",
						"name" => "advertisementTitle",
						"id" => "advertisement-title"
					])
					@include("layouts.form.textarea", [
						"label" => "Anzeigenbeschreibung",
						"name" => "advertisementDescription",
						"id" => "advertisement-descrtiption",
						"rows" => 10
					])
					@include("layouts.form.input", [
						"label" => "Video-Tag (Youtube)",
						"name" => "advertisementVideoTag",
						"id" => "advertisement-video-tag"
					])
				</div>
				<div>
					
				</div>
			</div>
			@include("layouts.form.input", [
				"type" => "submit",
				"value" => "Speichern",
				"class" => "btn btn-save"
			])
		</div>
	</form>
	</section>
@endsection