@extends('layouts.app')

@section('content')
	<section class="advertisement-show">
		<h1>{{$company->name}}</h1>
		@include("layouts.elements.video", [
			"youtube" => $advertisement->video,
			"height" => "700px"
		])

		<div class="details">
			<div>
				<h2>Informationen</h2>
				<p>Mitarbeiter bzw Mitglieder: {{$company->size}}</p>
				<p>{{$company->address}}</p>
				<p>{{$company->postcode}} {{$company->city}}</p>
				@if($company->phone)
				<p>Telefon: <a href="tel:{{$company->phone}}">{{$company->phone}}</a></p>
				@endif
				@if($company->email)
				<p>Email: <a href="mailto:{{$company->email}}">{{$company->email}}</a></p>
				@endif
			</div>
			<div>
				<h2>{{$advertisement->title}}</h2>
				@if($company->logo)<img src="{{$company->logo}}" alt="{{$company->name}}" />@endif
				<p>{{$advertisement->description}}</p>
			</div>
		</div>

		@include("layouts.elements.button", [
			"value" => "zurück zur Übersicht",
			"route" => "advertisement",
			"class" => "btn"
		])
	</section>
@endsection