@if (isset($filter) && !$filter):
<div class="filter">
	<form action="" method="">
		<div class="form-group">
			<label>Branche</label>
			<br/>
			<select name="branch" class="form-control">
				<option value="">Bitte wählen ...</option>
			</select>
		</div>
		<div class="form-group">
			<label>Postleitzahl</label>
			<br/>
			<select name="postcode" class="form-control">
				<option value="">Bitte wählen ...</option>
			</select>
		</div>
		<div class="form-group">
			<label>Bewertung</label>
			<br/>
			<select name="rating" class="form-control">
				<option value="">Bitte wählen ...</option>
			</select>
		</div>
	</form>
</div>
@endif