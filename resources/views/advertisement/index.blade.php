@extends('layouts.app')

@section('content')
	<section class="masonry">
		@foreach($advertisements as $advertisement)
			@php
			$company = App\Models\Company::where("id", $advertisement->companyId)->first();
			@endphp
			<div class="item">
				<div class="content">
					<h2>{{$company->name}}</h2>
					@if($company->logo)<img src="{{$company->logo}}" alt="{{$company->name}}" />@endif
					<h3>{{$advertisement->title}}</h3>
					<p>{{$advertisement->description}}</p>
					@include("layouts.elements.button", [
						"value" => "mehr ...",
						"route" => "anzeige-betrachten",
						"params" => [
							$advertisement->id,
							App\Models\Seo::seoFriendly($company->name)
						],
						"class" => "btn float-right"
					])
				</div>
			</div>
		@endforeach
	</section>
@endsection