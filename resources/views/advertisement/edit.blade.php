@extends('layouts.app')

@section('content')
	<section>
		<form method="POST" action="{{route("anzeige-speichern")}}">
			@csrf
			<div class="columns-two">
				<div class="mobile-order-2">
					@include("layouts.form.input", [
								"type" => "hidden",
								"name" => "advertisementId",
								"id" => "advertisement-id",
								"value" => $advertisement["id"]
							])
					@include("layouts.form.select", [
						"label" => "Firma oder Verein",
						"name" => "companyId",
						"id" => "company-id",
						"options" => $companyNames,
						"preSelect" => $advertisement->companyId
					])
					@include("layouts.form.input", [
						"label" => "Anzeigentitel",
						"name" => "advertisementTitle",
						"id" => "advertisement-title",
						"value" => $advertisement->title
					])
					@include("layouts.form.textarea", [
						"label" => "Anzeigenbeschreibung",
						"name" => "advertisementDescription",
						"id" => "advertisement-descrtiption",
						"rows" => 10,
						"value" => $advertisement->description
					])
					@include("layouts.form.input", [
						"label" => "Video-Tag (Youtube)",
						"name" => "advertisementVideoTag",
						"id" => "advertisement-video-tag",
						"value" => $advertisement->video
					])
					@include("layouts.elements.alert", [
						"message" => "Achtung
						Die Einschränkung des Zeitraums kann nach dem Speichern nicht mehr geändert werden.
						Die gesamte Anzeige wird nach Ablauf des 'bis'-Datum, falls gesetzt, automatisch deaktiviert"
					])
					@include("layouts.form.input", [
						"type" => "date",
						"label" => "Anzeige online von",
						"name" => "advertisementOnlineFrom",
						"id" => "advertisement-online-from",
						"value" => $advertisement->onlineFrom,
						"attrs" => [
							"min" => Carbon\Carbon::now()->format("Y-m-d"),
							// "disabled" => App\Models\Advertisement::disableDateInput($advertisement->onlineFrom)
						]
					])
					@include("layouts.form.input", [
						"type" => "date",
						"label" => "Anzeige online bis",
						"name" => "advertisementOnlineTo",
						"id" => "advertisement-online-to",
						"value" => $advertisement->onlineTo,
						"attrs" => [
							// "disabled" => App\Models\Advertisement::disableDateInput($advertisement->onlineTo)
						]
					])
				</div>
				<div class="flex center-center mobile-order-1">
					@include("layouts.elements.video", [
						"youtube" => $advertisement->video
					])
				</div>
			</div>
			@include("layouts.form.input", [
				"type" => "submit",
				"value" => "Speichern",
				"class" => "btn btn-save"
			])
		</form>

		@include("layouts.elements.button", [
			"value" => "zurück zur Übersicht",
			"class" => "btn btn-back",
			"route" => "profil"
		])
		
	</section>
@endsection