<article>
	<h2>Meine persönlichen Daten</h2>

	@if(Auth::check())
		<div class="card small">
			<div class="card-item">
				<label>Name:</label>
				{{ Auth::user()->name }}
			</div>
			<div class="card-item">
				<label>Email:</label>
				{{ Auth::user()->email }}
			</div>
			<div class="card-item">
				<label>Account erstellt am:</label>
				{{ Auth::user()->created_at->format("d.m.Y") }}
			</div>
		</div>
	@else
		@include("layouts.elements.alert", [
			"type" => "error",
			"message" => "Es tut uns leid!
			Sie haben keine Berechtigung, diese Inhalte zu sehen!"
		])
	@endif
</article>