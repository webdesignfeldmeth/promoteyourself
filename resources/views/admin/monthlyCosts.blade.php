@if(Auth::check() && $costs > 0)
<section class="monthly-costs">
	monatliche Kosten: <strong>{{number_format($costs, 2)}} &euro;</strong>
</section>
@endif