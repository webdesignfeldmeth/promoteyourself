<article>
	<h2>Meine Anzeigen</h2>

	<a href="/profil/neue-anzeige" class="btn">Neue Anzeige anlegen</a>

	<div class="list">
		@isset($advertisements)
			@foreach ($advertisements as $advertisement)
				<div class="list-item">
					<div>{{ $advertisement->title }}</div>
					<div>{{ $advertisement->description }}</div>
					<div>{{ $advertisement->video }}</div>
					<div class="flex end">
						@include("layouts.svg.svgLink", [
							"icon" => "edit",
							"title" => "Bearbeiten",
							"route" => "anzeige-bearbeiten",
							"params" => $advertisement->id,
							"class" => "btn btn-edit"
						])
						@if($advertisement->active)
							@include("layouts.svg.svgLink", [
								"icon" => "lock",
								"title" => "Inaktiv setzen",
								"route" => "anzeige-inaktiv",
								"params" => $advertisement->id,
								"class" => "btn"
							])
						@else
							@include("layouts.svg.svgLink", [
								"icon" => "unlock",
								"title" => "Aktiv setzen",
								"route" => "anzeige-aktiv",
								"params" => $advertisement->id,
								"class" => "btn"
							])
						@endif
						@include("layouts.svg.svgLink", [
							"icon" => "delete",
							"title" => "Löschen",
							"route" => "anzeige-loeschen",
							"params" => $advertisement->id,
							"class" => "btn btn-delete"
						])
					</div>
				</div>
			@endforeach
		@endisset
	</div>
</article>