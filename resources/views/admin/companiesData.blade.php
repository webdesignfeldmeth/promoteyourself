<article>
	<h2>Meine Firmen und Vereine</h2>
	
	<a href="/profil/neue-firma-oder-verein" class="btn">Firma oder Verein anlegen</a>

	<div class="list">
		@isset($companies)
			@foreach ($companies as $company)
				<div class="list-item">
					<div>{{ $company->name }}</div>
					<div>{{ $company->size }}</div>
					<div>{{ $company->address }}</div>
					<div class="flex end">
						@include("layouts.svg.svgLink", [
							"icon" => "edit",
							"title" => "Bearbeiten",
							"route" => "firma-oder-verein-bearbeiten",
							"params" => $company->id,
							"class" => "btn btn-edit"
						])
						@if($company->active)
							@include("layouts.svg.svgLink", [
								"icon" => "lock",
								"title" => "Inaktiv setzen",
								"route" => "firma-oder-verein-inaktiv",
								"params" => $company->id,
								"class" => "btn"
							])
						@else
							@include("layouts.svg.svgLink", [
								"icon" => "unlock",
								"title" => "Aktiv setzen",
								"route" => "firma-oder-verein-aktiv",
								"params" => $company->id,
								"class" => "btn"
							])
						@endif
						@include("layouts.svg.svgLink", [
							"icon" => "delete",
							"title" => "Löschen",
							"route" => "firma-oder-verein-loeschen",
							"params" => $company->id,
							"class" => "btn btn-delete"
						])
					</div>
				</div>
			@endforeach
		@endisset
	</div>
	
</article>