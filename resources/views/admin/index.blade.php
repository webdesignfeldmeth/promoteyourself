@extends('layouts.app')

@section('content')
	<section>
		@if(Auth::check())
			<div class="columns-two">
				<div>
					@include('admin.personalData')
					@include('admin.mailbox')
				</div>
				<div>
					@include('admin.companiesData')
					@include('admin.advertisements')
				</div>
			</div>
		@else
			@include("layouts.elements.alert", [
				"type" => "error",
				"size" => "medium",
				"headlineLarge" => "Hoppla, da ist was schief gelaufen!",
				"headlineMedium" => "Nicht authorisierter Zugriff",
				"message" => "Sie haben keine Berechtigung, diese Inhalte zu sehen!",
			])
		@endif
	</section>
@endsection