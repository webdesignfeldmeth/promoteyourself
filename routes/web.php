<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [App\Http\Controllers\HomeController::class, 'index'])->name('advertisement');

Auth::routes();

Route::get('/anzeige/{id}/{company}', [App\Http\Controllers\AdvertisementController::class, 'show'])
	->name('anzeige-betrachten');
Route::get('/profil', [App\Http\Controllers\AdminController::class, 'index'])
	->name('profil');

Route::group(['middleware' => ['auth']], function() {
	Route::get('/profil/neue-anzeige', [App\Http\Controllers\AdvertisementController::class, 'create'])
		->name('neue-anzeige');
	Route::get('/profil/anzeige/bearbeiten/{id}', [App\Http\Controllers\AdvertisementController::class, 'edit'])
		->name('anzeige-bearbeiten');
	Route::post('/profil/anzeige/speichern', [App\Http\Controllers\AdvertisementController::class, 'update'])
		->name('anzeige-speichern');
	Route::post('/profil/neue-anzeige/speichern', [App\Http\Controllers\AdvertisementController::class, 'store'])
		->name('neue-anzeige-speichern');
	Route::get('/profil/anzeige/inaktiv/{id}', [App\Http\Controllers\AdvertisementController::class, 'active'])
		->name('anzeige-inaktiv');
	Route::get('/profil/anzeige/aktiv/{id}', [App\Http\Controllers\AdvertisementController::class, 'active'])
		->name('anzeige-aktiv');
	Route::get('/profil/anzeige/loeschen/{id}', [App\Http\Controllers\AdvertisementController::class, 'delete'])
		->name('anzeige-loeschen');

	Route::get('/profil/neue-firma-oder-verein', [App\Http\Controllers\CompanyController::class, 'create'])
		->name('neue-firma-oder-verein');
	Route::get('/profil/firma-oder-verein/bearbeiten/{id}', [App\Http\Controllers\CompanyController::class, 'edit'])
		->name('firma-oder-verein-bearbeiten');
	Route::post('/profil/firma-oder-verein/speichern', [App\Http\Controllers\CompanyController::class, 'update'])
		->name('firma-oder-verein-speichern');
	Route::get('/profil/neue-firma-oder-verein/inaktiv/{id}', [App\Http\Controllers\CompanyController::class, 'active'])
		->name('firma-oder-verein-inaktiv');
	Route::get('/profil/neue-firma-oder-verein/aktiv/{id}', [App\Http\Controllers\CompanyController::class, 'active'])
		->name('firma-oder-verein-aktiv');
	Route::get('/profil/neue-firma-oder-verein/loeschen/{id}', [App\Http\Controllers\CompanyController::class, 'delete'])
		->name('firma-oder-verein-loeschen');
	Route::post('/profil/neue-firma-oder-verein/speichern', [App\Http\Controllers\CompanyController::class, 'store'])
		->name('neue-firma-oder-verein-speichern');
});

