<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

class Admin extends Model
{
    use HasFactory;
    
    const COSTS_COMPANY = 4.99;
    const COSTS_ADVERTISEMENT = 7.99;

    public static function getTotalCosts() {
        $costs = 0;

        if(Auth::check()) {
            $userId = Auth::user()->id;

            $datas = Advertisement::where("userId", $userId)->where("active", 1);
            $costs += self::costsAdvertisements($userId, $datas, self::COSTS_ADVERTISEMENT);
            $datas = Company::where("userId", $userId)->where("active", 1);
            $costs += self::costsAdvertisements($userId, $datas, self::COSTS_COMPANY);
        }


        return $costs;
    }

    protected static function costsCompanies() {

    }

    protected static function costsAdvertisements($userId, $datas, $amount) {
        $costs = [];

        foreach($datas->get() as $data) {
            if(self::isFullMonth(
                $data->created_at->day,
                $data->created_at->month,
                $data->created_at->year)) {
                $costs[] += $amount;
            } else {
                $allDays = date("t", mktime(0, 0, 0, date("m", time()), 1, date("Y", time())));
                $costs[] += ($amount / $allDays) * Advertisement::getPastDays($data);
            }
        }

        $costs = array_sum($costs);

        return $costs; 
    }

    protected static function isFullMonth($day, $month, $year) {
        return mktime(0, 0, 0, date("m", time()), 1, date("Y", time())) == mktime(0, 0, 0, $month, $day, $year) 
                ? true 
                : false;
    }

}
