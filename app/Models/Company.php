<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Company extends Model
{
    use HasFactory;

    public static function getCompanyNamesAsList($companies) {
        $companyNames = [];

        foreach($companies as $company) {
            $companyNames[$company->id] = $company->name;
        }

        ksort($companyNames);

        return $companyNames;
    }
}
