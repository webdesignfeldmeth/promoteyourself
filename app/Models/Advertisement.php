<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Advertisement extends Model
{
    use HasFactory;

    /**
     * Deaktivieren des Datumsfeldes, wenn ein Wert gesetzt wurde
     */
    public static function disableDateInput($onlineFrom) {
        return $onlineFrom == null ? "no" : "disabled";
    }

    /**
     * Prüfen, wo der Start- bzw. Endzeitraum dem aktuellen Monat und Jahr 
     * entspricht
     */
    public static function isSameMonthAndYear($onlineDate) {
        $isSame = false;
        if($onlineDate == date("Y-m", time())) {
            $isSame = true;
        }
        return $isSame;
    }

    public static function getPastDays($data) {
        // Wenn es nur das Datum der Datensatzerstellung gibt
        // Prüfen, ob es sich im selben Monat befindet
        $daysFromMonthStart = 0;
        if(date("Y-m", time()) == $data->created_at->format("Y-m")) {
            $daysFromMonthStart = date("d", strtotime($data->created_at->day));
        }
        // Wenn es Datum bei "Anzeige online von" gibt
        // Prüfen, ob es sich im selben Monat befindet
        if(date("Y-m", time()) == date("Y-m", strtotime($data->onlineFrom))) {
            $daysFromMonthStart = date("d", strtotime($data->onlineFrom));
        }

        // Wenn es Datum bei "Anzeige online bis" gibt
        // Prüfen, ob es sich im selben Monat befindet
        $daysToMonthEnd = 0;
        if(date("Y-m", time()) == date("Y-m", strtotime($data->onlineTo))) {
            $daysToMonthEnd = date("t") - date("d", strtotime($data->onlineTo));
        }

        $pastDays = date("t") - $daysFromMonthStart - $daysToMonthEnd;

        return $pastDays;
    }
}
