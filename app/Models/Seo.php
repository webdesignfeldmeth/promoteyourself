<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Seo extends Model
{
	use HasFactory;

	public static function seoFriendly($string) {
		$url = strtolower($string);
		$url = str_replace(
			[" "],
			["-"],
			$url
		);
		return $url;
	}
}