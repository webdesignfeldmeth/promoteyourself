<?php

namespace App\Http\Controllers;

use App\Models\Advertisement;
use App\Models\Company;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class AdvertisementController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('advertisement.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $headline = "Neue Anzeige anlegen";
        $companyNames = Company::getCompanyNamesAsList(Company::where("userId", User::getUserId())->get());

        return view('advertisement.create', compact(
            "headline",
            "companyNames"
        ));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $advertisement = new Advertisement();
        
        $advertisement->userId = Auth::user()->id;
        $advertisement->companyId = $request->get('companyId');
        $advertisement->title = $request->get('advertisementTitle');
        $advertisement->description = $request->get('advertisementDescription');
        $advertisement->video = $request->get('advertisementVideoTag');
        $advertisement->active = 1;
        
        $advertisement->save();

        return redirect('/profil');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Advertisement  $advertisement
     * @return \Illuminate\Http\Response
     */
    public function show(Advertisement $advertisement, $id, $key)
    {
        $headline = "Neue Anzeige anlegen";
        $advertisement = Advertisement::where("id", $id)->first();
        $company = Company::where("id", $advertisement->companyId)->first();

        return view('advertisement.show', compact(
            "headline",
            "advertisement",
            "company"
        ));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Advertisement  $advertisement
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, Advertisement $advertisement)
    {
        $headline = "Anzeige bearbeiten";
        $advertisementId = $request->route()->parameters()["id"];
        $advertisement = Advertisement::where("id", $advertisementId)->first();
        $companyNames = Company::getCompanyNamesAsList(Company::where("userId", User::getUserId())->get());

        return view('advertisement.edit', compact(
            "headline",
            "advertisement",
            "companyNames"
        ));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Advertisement  $advertisement
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Advertisement $advertisement)
    {
        $advertisementId = intval($request->get("advertisementId"));
        $advertisement = $advertisement->where("id", $advertisementId)->first();
        $advertisement->companyId = $request->get("companyId");
        $advertisement->title = $request->get("advertisementTitle");
        $advertisement->description = $request->get("advertisementDescription");
        $advertisement->video = $request->get("advertisementVideoTag");
        $advertisement->onlineFrom = $request->get("advertisementOnlineFrom");
        $advertisement->onlineTo = $request->get("advertisementOnlineTo");

        $advertisement->save();
        
        return redirect()->route('anzeige-bearbeiten', ["id" => $advertisementId]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Advertisement  $advertisement
     * @return \Illuminate\Http\Response
     */
    public function destroy(Advertisement $advertisement)
    {
        //
    }

    /**
     * Set active state
     */
    public function active(Request $request) {
        $advertisementId = $request->route()->parameters()["id"];
        $advertisement = Advertisement::where("id", $advertisementId)->first();
        $active = preg_match("/inaktiv/", $request->getPathInfo()) ? 0 : 1;
        
        $advertisement->active = $active;
        $advertisement->save();
        
        return redirect('/profil');
    }
}
