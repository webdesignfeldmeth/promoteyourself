<?php

namespace App\Http\Controllers;

use App\Models\Advertisement;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $allAdvertisements = [];
        $advertisements = Advertisement::where("active", 1)->get();

        return view('advertisement.index', compact(
            "advertisements"
        ));
    }

}