<?php

namespace App\Http\Controllers;

use App\Models\Company;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class CompanyController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $headline = "Neue Firma oder Verein anlegen";

        return view('company.create', compact(
            "headline"
        ));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $company = new Company();
        
        $company->userId = Auth::user()->id;
        $company->companyLogoUpload = "";
        $company->companyName = $request->get('companyName');
        $company->companySize = $request->get('companySize');
        $company->companyAddress = $request->get('companyAddress');
        $company->companyPostcode = $request->get('companyPostcode');
        $company->companyCity = $request->get('companyCity');
        $company->companyPhone = $request->get('companyPhone');
        $company->companyEmail = $request->get('companyEmail');

        $company->save();

        return redirect('/profil');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Company  $company
     * @return \Illuminate\Http\Response
     */
    public function show(Company $company)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Company  $company
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, Company $company)
    {
        $headline = "Firma oder Verein bearbeiten";
        $companyId = $request->route()->parameters()["id"];
        $company = Company::where("id", $companyId)->first();

        return view('company.edit', compact(
            "headline",
            "company"
        ));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Company  $company
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Company $company)
    {
        $company = $company->where("id", intval($request->get("companyId")))->first();
        
        $company->companyName = $request->get("companyName");
        $company->companySize = $request->get("companySize");
        $company->companyAddress = $request->get("companyAddress");
        $company->companyPostcode = $request->get("companyPostcode");
        $company->companyCity = $request->get("companyCity");
        $company->companyPhone = $request->get("companyPhone");
        $company->companyEmail = $request->get("companyEmail");

        $company->save();
        
        return redirect('/profil');
    }

    /**
     * Delete the specified company from database
     */
    public function delete(Request $request, Company $company) {
        $companyId = $request->route()->parameters()["id"];
        Company::where("id", $companyId)->delete();

        return redirect('/profil');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Company  $company
     * @return \Illuminate\Http\Response
     */
    public function destroy(Company $company)
    {
        //
    }

    /**
     * Set active state
     */
    public function active(Request $request) {
        $companyId = $request->route()->parameters()["id"];
        $company = Company::where("id", $companyId)->first();
        $active = preg_match("/inaktiv/", $request->getPathInfo()) ? 0 : 1;
        
        $company->active = $active;
        $company->save();
        
        return redirect('/profil');
    }

}
